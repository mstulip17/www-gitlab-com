---
layout: handbook-page-toc
title: "Enterprise Area Sales Manager Operating Rhythm"
description: "Successful management includes onboarding, reviewing Command Plans, opportunity coaching, strategic coaching, career development and performance management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
As a compliment to the [Enterprise ASM Manager Operating Rhythm (MOR) 1-pager](https://docs.google.com/spreadsheets/d/1s_QMqt4a3UKKZkIdf82N5840_ygPE4z7mpgpniSCsNc/edit?usp=sharing), the below content provides more granular guidance into how each of these responsibilities is conducted. The content has been sourced from experienced GitLab ASMs and is meant to serve as a guide (not hard and fast mandates) to help you manage your business and optimize results.

## Planning and Execution

### Prioritization
Account ranking helps to ensure sales team members are focused on accounts and opportunities that have the highest potential and not spread themselves too thin across too many different account pursuits. [Landed Addressable Market (LAM)](/handbook/sales/sales-term-glossary/#landed-addressable-market-lam) data is used to help inform this prioritization. Each Enterprise Area Sales Manager (ASM) should take the time up front to rank accounts. While this is a material up-front time investment, it’s incredibly helpful in better understanding your territory and is much easier to update and iterate on over time. Account ranking should be revisited at least quarterly (if not more frequently).

**Suggested Steps**
1. Stack-rank accounts based on LAM
    - Take into consideration other inputs (e.g. sales team and partner territory and/or account insights, etc.)
    - No list will be perfect, so focus on an account ranking that you feel is good enough
1. Look at how those accounts are distributed across the team and try to ensure a fairly equitable distribution across your Strategic Account Leaders (SALs)
1. Have each SAL identify their top 5-10 targeted Expand accounts and top 5-10 targeted Land accounts
1. Next, have each SAL tier their accounts as follows:
    - Tier 1: Top 2-3 targeted Expand accounts and top 2-3 targeted Land accounts (these are the accounts typically featured during QBRs)
    - Tier 2: Target accounts (from the list of 10-20 above) not on the Tier 1 list
        - SALs conduct periodic outreach to these accounts to keep them warm and will update their tier and ranking if and when a potential sales opportunity is identified
        - ASMs should consider whether progress is not being made with these accounts due to time/resource constraints and whether it makes sense to reallocate these accounts, hire additional resources, or leave things as-is
1. Have each SAL map their Tier 1 and 2 accounts into a 2x2 matrix with Effort on the y-axis and Value/Outcome on the x-axis
    - Top right quadrant (High Effort, High Value): Tier 1 and 2 Growth accounts
    - Top left quadrant (High Effort, Low _Initial_ Value): Tier 1 and 2 Land accounts
    - Bottom right quadrant (Low Effort, High Value): Starter and flat renewals
    - Bottom left quadrant (Low Effort, Low Value): Deprioritize everything that falls here
1. Create a consolidated 2x2 matrix that is a roll-up of all of your SALs' matrices
    - A matrix may be created for your entire territory or a sub-territory
    - Use and iterate on this matrix with your team regularly (weekly if not daily) 

### Account Reviews
Accounts reviews are conducted quarterly (or more frequently as needed) at various points in the quarter, usually over a 2-week period. The extended account team (SAL, SA, TAM, SDR) is encouraged to participate and contribute. TAM participation is critical to ensure alignment between the account plan and the customer success plan (see [What is the Difference Between an Account Plan and a Success Plan?](/handbook/sales/account-planning/#what-is-the-difference-between-an-account-plan-and-a-success-plan)). Initially, ASMs and TAM Managers may want or need to partner on scheduling and facilitating these reviews, but regular account reviews should eventually become part of an account team's normal operating rhythm. See [Coaching Account Planning for ASMs](/handbook/sales/account-planning/coaching/) for additional guidance.

### Partner Reviews
Partner reviews are typically bi-weekly or monthly, led by the regional Channel & Alliances team, and supported by a Partner Plan and Review document. Recommended participants include key reguinal Channel & Alliances team members, ASMs, and the Regional Director (RD). The focus of these reviews is to ensure the partner plan is aligned with regional sales goals and objectives and covers questions like:
- Do we have the right set of partners to support the business?
- Where and what type of additional partners are needed and why? 
- How do we drive even more alignment and collaboration between key partners and GitLab account teams? 

### Opportunity Reviews
ASMs should be intimately familiar with every key opportunity that his or her team is actively managing. During monthly forecast calls and weekly 1x1s, the ASM and each SAL identify which opportunities would benefit from an opportunity consult and work together to get those scheduled. When possible, the entire account team (SAL, SA, TAM, and SDR) participates in these reviews. ASMs should be conducting multiple opportunity consults every week (though not necessarily with the same SALs). Sometimes these reviews follow the [standard opportunity consult format](/handbook/sales/command-of-the-message/opportunity-consults/). Other times, these reviews are working sessions that include the ASM working with the SAL and/or account team to collaboratively complete the Command Plan for a given opportunity. And other times, the ASM may choose to hone in on one or a few specific areas of the opportunity (this may especially be the case for follow up opportunity reviews).

Some ASMs also choose to conduct group opportunity reviews to incorporate peer review and feedback (either during regularly scheduled team meetings or as separate meetings). Consider the below framework and iterate as you deem appropriate.
- Invite SALs to volunteer in advance to present their Command Plan for a key deal to the rest of the team
- Schedule a 30-45 minute meeting once you have a volunteer
    - ASM to decide whether team participation is mandatory or optional (consider starting as optional but strongly encouraged until the value is proven)
- During the meeting, the volunteer SAL presents his or her Command Plan for one key opportunity and then receives questions, input, and feedback from the team
- If successful, consider creating a recurring meeting series (1-2x per month)

### Forecasting and Pipeline Reviews
The following forecast & pipeline review cadence is recommended:
1. Month 1 of a quarter: current fiscal quarter (CFQ)
1. Month 2 of a quarter: next fiscal quarter (NFQ)
1. Month 3 of a quarter: two fiscal quarters out (NFQ+1)

Quarterly Business Reviews (QBRs) provide a great opportunity to conduct Month 1 CFQ forecast & pipeline reviews such that a separate review need not be scheduled. At the end of Month 1, the ASM should encourage SALs to clear out pipeline opportunities from the CFQ forecast. Entering Month 2, all CFQ opportunities should be in either Best Case or Commit. If they are not, the forecasted close date should be pushed out.

Month 2 and Month 3 forecast & pipeline reviews are typically scheduled for 2 hours. Ideally, the Month 2 review takes place ~30 days after the QBRs, and Month 3 reviews are scheduled about ~30 days after that (but not in the last 7-10 days of the quarter). See the Out-Quarter Forecasting and Pipeline Reviews section below for additional guidance.

#### In-Quarter Forecasting
Every week, ASMs must review the team's Commit & Best Case opportunities and submit their forecast. ASMs should work with their teams to ensure that the Single Source of Truth (SSOT) for Best Case and Commit deals is in Salesforce.

**Key Resources**
- Clari dashboards
    - [Sales Forecast Dashboard](https://app.clari.com/dashboard?dashboardId=606f0ac5ac043e61f9ebeb8d)
    - [Rep/Manager 1x1 Dashboard](https://app.clari.com/dashboard?dashboardId=605a14753f9d77039390026f)
- Salesforce dashboards
    - Current Quarter (CQ) Sales Dashboard (link varies by region; see [Enterprise Dashboards](/handbook/sales/field-operations/sales-operations/#sales-operations-sponsored-dashboards-and-maintenance))

#### Out-Quarter Forecasting and Pipeline Reviews
As outlined above, out-quarter reviews typically take place in Month 2 and 3 of the quarter. Each SAL has 15-20 minutes to cover the following topics:
1. Forecast
    - Summary of Stage 3+ opportunities by stage
    - Churn risks
    - Call out specific opportunities only if you want to discuss (e.g. highlight a risk, ask for help, etc.)
1. Pipeline
    - Where are you against pipeline coverage targets?
    - Plans to close gaps, highlight risks
    - Actions with partners and SDRs
1. Prioritized Actions

ASMs will need to manage the agenda to ensure the team doesn't get bogged down in too much detail and encourage SALs to focus on the bigger picture. More specifically, even the best SALs will miss their quarterly quota from time to time. What's important is that the SAL and ASM are aware of the risk in advance and that there is sufficient pipeline in other quarters to make up for a down quarter. SALs should be empowered to be the CEO of their own territory and manage their business accordingly. ASMs should also continuously reinforce that the dashboards, heat map, and other resources listed below are for the SAL's benefit (not management tools) and to help them effectively manage their business. The NFQ+1 forecast and pipeline reviews that take place in Month 3 of a quarter are typically even higher level and not as detailed (as to be expected for opportunities not expected to close for several months).

**Key Resources**
- Clari dashboards
    - [Pipeline Health Dashboard](https://app.clari.com/dashboard?dashboardId=5b96eb8f9258b900014297ad)
    - [Rep/Manager 1x1 Dashboard](https://app.clari.com/dashboard?dashboardId=605a14753f9d77039390026f)
- Salesforce dashboards
    - Pipeline Dashboard (link varies by region; see [Enterprise Dashboards](/handbook/sales/field-operations/sales-operations/#sales-operations-sponsored-dashboards-and-maintenance))
    - Pipeline Health Check (same as above)
- SAL Heat Map

### Sales Calls
ASMs should routinely participate in sales calls with their SALs including:
- Helping with meeting prep
    - See [Develop a Pre-Call Plan](/handbook/sales/playbook/discovery/#develop-a-pre-call-plan)
- Participating on the call 
- Debriefing immediately after the call

ASMs should provide coaching and mentoring in a collaborative and proactive way. Examples include but are not limited to: 
- Reinforce [effective discovery](/handbook/sales/playbook/discovery/) best practices
- Maintain proper focus on customer value ([Command of the Message](/handbook/sales/command-of-the-message/)) and avoid technical rabbit holes
- Identify when and how to position GitLab's holistic differentiators to the right people at the right time
- Help newer SALs understand the context behind industry terms/concepts like application refactoring, microservices, Kubernetes, and more
- Improve the fidelity of [Command Plans](/handbook/sales/command-of-the-message/command-plan/)

### GTM Planning
Annual GTM planning includes recommending territory assignments and quotas and identifying resource requirements to meet and exceed financial targets.

### Quarterly Business Reviews
See [GitLab Sales QBRs](/handbook/sales/qbrs/). QBR decks are stored [here](https://drive.google.com/drive/u/0/folders/1QAWFYxT2-NzWpOUodepbl0O6zfsMxJGL) (GitLab internal only).

## Talent Management

### Recruiting
Always be recruiting! As outlined in the [Winning Top Talent](/handbook/sales/field-manager-development/#winning-top-talent) portion of the GitLab Field Manager Development Program, the importance of effective recruiting to GitLab's continued growth and success cannot be understated. ASMs should meet regularly with your Talent Acquisition Recruiter (exact cadence will vary based on hiring needs) and should also be comfortable and confident in selling the amazing opportunity here at GitLab.

**Key Resources**    
- Success Profile
- Interview Guide
- "Why GitLab" Summary

### Onboarding
ASMs should work with new SALs to develop and monitor progress against a documented 30/60/90 day plan, set clear performance expectations, and provide regular feedback during the onboarding process. ASMs should encourage new SALs to complete their general GitLab onboarding issue and also ensure that they actively participate in and complete [Sales Quick Start](/handbook/sales/onboarding/).

### Performance Management
Formal individual performance assessments are not conducted in the Enterprise sales team at this time.

### Developing and Retaining
Developing and retaining key talent is constantly top-of-mind for high-performing ASMs. The best ASMs provide regular [coaching](/handbook/leadership/coaching/), follow [Performance Management](/handbook/sales/field-manager-development/#performance-management-and-giving-feedback) best practices, and leverage GitLab's [Field Functional Competencies](/handbook/sales/training/field-functional-competencies/) to reinforce the critical skills and behaviors that will lead to desired outcomes/results.

### Team Assessment
Last but not least, ASMs also assess their teams annually following the [Performance / Growth Matrix](/handbook/people-group/talent-assessment/#the-performancegrowth-matrix).
