---
layout: job_family_page
title: "Engineering Analytics Manager"
---

Engineering Analytics Manager manages a team of Engineering Analysts that work on data capabilities for the Engineering Division. 

## Responsibilities 

* Define the tools and process to deliver data-driven insights for the Engineering Division.
* Define and maintain Engineering Division's data visualizations guidelines and standards.
* Ensure accuracy of Performance Indicators in the Engineering Division and all it's departments.
* Drive efficiency of Engineering Division's dashboards and metrics.
* Author team's quarterly OKRs and project plans to deliver on them.
* Run Engineering Analytics agile process and stand-up.
* Plan Engineering Analytics's long-term tasks and align with Quality Department's direction and vision.
* Hire and growth a team of Engineering Analysts.
* Provide guidance and coaching to Engineering Analysts.
* Collaborate closely with GitLab’s Data team.

## Requirements

* [Self-motivated and self-managing](https://about.gitlab.com/handbook/values/#efficiency), with strong organizational skills.
* Share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.
* A track record of using quantitative analysis to generate insights, and drive better organizational decision making.
* Track record of solving scaling challenges and high growth optimizations with positive impact to the business.
* 5 years or more experience with hands-on SQL and relational databases.
* 2 years or more experience in leadership position managing an analytics team.
* Experience with Data Visualization Tools (E.g. Sisense, Looker, Tableau)
* Experience with Data warehouses (e.g. Snowflake)
* Clear and concise communication, able to convey complex business logic, and analytical recommendations to stakeholders.
* Proficiency in the English language, with excellent written and oral communication skills.
* Working knowledge of Git and source control.
* Working knowledge of basic Ruby scripting.
* Working knowledge of HTML.
* Able to thrive in a fully remote organization.
* Able to use GitLab.
* BS degree in Computer Science, Finance, Accounting or Economics (MS Prefered)
* Confidentiality in handling sensitive organizational information.
* Successful completion of our [background check](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#background-checks)


### Nice to haves
* Experience in a peak performance organization
* Enterprise software company experience.
* Developer platform/tool industry experience.

## Levels 
### Engineering Analytics Manager 

#### Engineering Analytics Manager Job Grade

The Engineering Analytics Manager  is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Engineering Analytics Manager Responsibilities

* Author team quarterly OKRs and drive them to completion.
* Align Engineering Analytics team OKRs with Engineering Division's needs.
* Write detailed and actionable metrics proposals, delegate and drive them to completion. 
* Drive process and work stream efficiency that results in Engineering Analytics team's output.
* Participate in all of Engineering Key Reviews and ensure that the needs of Engineering leaders are met.
* Collaborate with Engineering leaders on business needs and capture them as requirements for the Engineering Analytics team.
* Listen and capture feedback from team members and prioritize actionable improvements.
* Own the hiring and team formation process of the Engineering Analytics team.
* Provide guidance to Engineering Analysts on technical and non-technical aspects. 
* Own project management, agile process of the Engineering Analytics team. 
* Collaborate with GitLab’s Data team on data dependencies for Engineering.
* Partner with GitLab’s Data team on training activities for Engineering Analysts.

## Performance Indicators 

* Engineering PI Content MR Rate 
* Handbook MR Rate
* Open Engineering Analytics Issue Age  

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to fill out a short questionnaire.
* Next, candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters.
* Next, candidates will be invited to schedule a first interview with the hiring manager.
* Next, candidates will be invited to schedule a second technical interview with an Engineering Analyst.
* Next, candidates will be invited to schedule a third interview with an additional member of the Engineering Analytics Team.
* Next, candidates will be invited to schedule a forth interview with a peer Engineering Manager in the Quality Department.
* Finally, candidates may be asked to interview with the VP of Quality.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.
